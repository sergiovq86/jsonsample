package com.tema5.myapplication;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.tema5.myapplication.adapters.HerosAdapter;
import com.tema5.myapplication.models.Hero;
import com.tema5.myapplication.models.Squad;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

/**
 * Created by Sergio on 04/05/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class HeroActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.squad_layout);

        LinearLayoutManager manager = new LinearLayoutManager(this);
        HerosAdapter adapter = new HerosAdapter(this);

        RecyclerView recyclerView = findViewById(R.id.recycler);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);

        String json = loadJSONFromAsset();
        if (json != null) {
            try {
                ArrayList<Hero> lista_heroes = new ArrayList<>();

                JSONObject jsonSquad = new JSONObject(json);
                JSONArray members = jsonSquad.getJSONArray("members");

                // recorremos el jsonarray
                for (int i = 0; i < members.length(); i++) {
                    JSONObject heroe = members.getJSONObject(i);
                    JSONArray powers = heroe.getJSONArray("powers");

                    ArrayList<String> poderes = new ArrayList<>();
                    for (int j = 0; j < powers.length(); j++) {
                        String unpoder = powers.getString(j);
                        poderes.add(unpoder);
                    }

                    String name = heroe.getString("name");
                    int age = heroe.getInt("age");
                    String id = heroe.getString("secretIdentity");

                    Hero hero = new Hero(name, id, age, poderes);
                    lista_heroes.add(hero);
                }

                adapter.setLista(lista_heroes);

                String squadName = jsonSquad.getString("squadName");
                String town = jsonSquad.getString("homeTown");
                int formed = jsonSquad.getInt("formed");
                String base = jsonSquad.getString("secretBase");
                boolean active = jsonSquad.getBoolean("active");

                Squad squad = new Squad(squadName, town, base, formed, active, lista_heroes);
                TextInputEditText etName = findViewById(R.id.etName);
                TextInputEditText etTown = findViewById(R.id.etTown);
                TextInputEditText etFormed = findViewById(R.id.etFormed);
                TextInputEditText etBase = findViewById(R.id.etBase);
                TextInputEditText etActive = findViewById(R.id.etActive);

                etName.setText(squad.getName());
                etTown.setText(squad.getTown());
                etFormed.setText(String.valueOf(squad.getFormed()));
                etBase.setText(squad.getBase());
                etActive.setText(String.valueOf(squad.isActive()));


            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this, "Error al parsear el JSON", Toast.LENGTH_SHORT).show();
        }
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("archivo.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


}
