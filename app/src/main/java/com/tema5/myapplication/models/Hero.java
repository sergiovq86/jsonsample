package com.tema5.myapplication.models;

import java.util.ArrayList;

/**
 * Created by Sergio on 04/05/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class Hero {

    private String name, secretIdentity;
    private int age;
    private ArrayList<String> powers;

    public Hero(String name, String secretIdentity, int age, ArrayList<String> powers) {
        this.name = name;
        this.secretIdentity = secretIdentity;
        this.age = age;
        this.powers = powers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecretIdentity() {
        return secretIdentity;
    }

    public void setSecretIdentity(String secretIdentity) {
        this.secretIdentity = secretIdentity;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public ArrayList<String> getPowers() {
        return powers;
    }

    public void setPowers(ArrayList<String> powers) {
        this.powers = powers;
    }
}
