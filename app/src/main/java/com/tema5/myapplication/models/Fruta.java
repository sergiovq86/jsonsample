package com.tema5.myapplication.models;

/**
 * Created by Sergio on 04/05/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class Fruta {

    private String nombre;
    private int unidades;

    public Fruta(String nombre, int unidades) {
        this.nombre = nombre;
        this.unidades = unidades;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getUnidades() {
        return unidades;
    }

    public void setUnidades(int unidades) {
        this.unidades = unidades;
    }
}
