package com.tema5.myapplication.models;

import java.util.ArrayList;

/**
 * Created by Sergio on 04/05/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class Squad {

    private String name, town, base;
    private int formed;
    private boolean active;
    private ArrayList<Hero> members;

    public Squad(String name, String town, String base, int formed, boolean active, ArrayList<Hero> members) {
        this.name = name;
        this.town = town;
        this.base = base;
        this.formed = formed;
        this.active = active;
        this.members = members;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public int getFormed() {
        return formed;
    }

    public void setFormed(int formed) {
        this.formed = formed;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public ArrayList<Hero> getMembers() {
        return members;
    }

    public void setMembers(ArrayList<Hero> members) {
        this.members = members;
    }
}
