package com.tema5.myapplication.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.tema5.myapplication.R;
import com.tema5.myapplication.models.Hero;

import java.util.ArrayList;

/**
 * Created by Sergio on 04/05/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class HerosAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final ArrayList<Hero> lista;
    private final Context context;

    public HerosAdapter(Context context) {
        this.context = context;
        lista = new ArrayList<>();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_hero, parent, false);
        return new HeroHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        HeroHolder fHolder = (HeroHolder) holder;

        Hero heroe = lista.get(position);
        fHolder.name.setText(heroe.getName());
        fHolder.age.setText(String.valueOf(heroe.getAge()));
        fHolder.id.setText(heroe.getSecretIdentity());

        for (String power : heroe.getPowers()) {
            TextView textView = new TextView(context);
            textView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            textView.setText(power);
            fHolder.powers.addView(textView);
        }
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    static class HeroHolder extends RecyclerView.ViewHolder {

        private final TextInputEditText name;
        private final TextInputEditText age;
        private final TextInputEditText id;
        private final LinearLayout powers;

        public HeroHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.etName);
            age = itemView.findViewById(R.id.etAge);
            id = itemView.findViewById(R.id.etSecret);
            powers = itemView.findViewById(R.id.powers);
        }
    }

    public void setLista(ArrayList<Hero> arrayList) {
        lista.clear();
        lista.addAll(arrayList);
        notifyDataSetChanged();
    }
}
