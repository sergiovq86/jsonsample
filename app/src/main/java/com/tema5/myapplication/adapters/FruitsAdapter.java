package com.tema5.myapplication.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tema5.myapplication.R;
import com.tema5.myapplication.models.Fruta;

import java.util.ArrayList;

/**
 * Created by Sergio on 04/05/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class FruitsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final ArrayList<Fruta> lista;

    public FruitsAdapter() {
        lista = new ArrayList<>();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_fruit, parent, false);
        return new FruitHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        FruitHolder fHolder = (FruitHolder) holder;

        Fruta estaFruta = lista.get(position);
        fHolder.name.setText(estaFruta.getNombre());
        fHolder.quantity.setText(String.valueOf(estaFruta.getUnidades()));
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    static class FruitHolder extends RecyclerView.ViewHolder {

        private final TextView name;
        private final TextView quantity;

        public FruitHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.tvName);
            quantity = itemView.findViewById(R.id.tvQuantity);
        }
    }

    public void setLista(ArrayList<Fruta> arrayList) {
        lista.clear();
        lista.addAll(arrayList);
        notifyDataSetChanged();
    }
}
