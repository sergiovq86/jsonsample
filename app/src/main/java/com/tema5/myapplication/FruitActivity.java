package com.tema5.myapplication;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tema5.myapplication.adapters.FruitsAdapter;
import com.tema5.myapplication.models.Fruta;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Sergio on 04/05/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class FruitActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_layout);

        LinearLayoutManager manager = new LinearLayoutManager(this);
        FruitsAdapter adapter = new FruitsAdapter();

        RecyclerView recyclerView = findViewById(R.id.recycler);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);

        String json = "{\n" +
                "   \"frutas\": [\n" +
                "       { \"nombre_fruta\":\"Manzana\" , \"cantidad\":10 }, \n" +
                "       { \"nombre_fruta\":\"Pera\" , \"cantidad\":20 }, \n" +
                "       { \"nombre_fruta\":\"Naranja\" , \"cantidad\":30 }\n" +
                "    ]\n" +
                "}\n";

        //inicializamos la lista donde almacenaremos los objetos Fruta
        ArrayList<Fruta> lista_frutas = new ArrayList<>();

        try {
            //Creamos un objeto JSON a partir del String llamado json
            JSONObject object = new JSONObject(json);
            JSONArray json_array = object.getJSONArray("frutas"); //obtenemos el array cuya clave es "frutas"

            // recorremos el jsonarray
            for (int i = 0; i < json_array.length(); i++) {
                // obtenemos la futa en cada posición y se guarda en objetofruta
                JSONObject objetofruta = json_array.getJSONObject(i);
                // se puede obtener cada valor del objeto con su clave
                String nombre = objetofruta.getString("nombre_fruta");
                int cantidad = objetofruta.getInt("cantidad");
                // por último, se crea un objeto fruta con nombre y cantidad y se añade al array
                lista_frutas.add(new Fruta(nombre, cantidad));
            }
            // lista_frutas es un arraylist con todas las frutas, ya podemos utilizarlo
            adapter.setLista(lista_frutas);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}
